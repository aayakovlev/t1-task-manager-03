# TASK MANAGER

## DEVELOPER

**NAME**: Yakovlev Anton

**E-MAIL**: aayakovlev@t1-consulting.ru

## SOFTWARE

**OS**: Windows 10 LTSC 1809 (build 17763.2628)

**JDK**: Oracle Java 1.8.0_301

## HARDWARE

**CPU**: AMD Ryzen 9 3900x

**RAM**: 32 Gb

**SSD**: NVMe 512 Gb

## RUN APPLICATION

````bash
java -jar ./task-manager.jar help
````
